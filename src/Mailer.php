<?php

class Mailer
{
    const STATUS_SUCCESS = 'success';
    const STATUS_FAILED = 'filed';

    // @todo set real name
    protected $username = '';
    // @todo set real pass
    protected $password = '';
    // @todo set real email
    protected $from = '';
    // @todo set real subject;
    protected $subject;

    /**
     * @var \Swift_Mailer
     */
    protected $mailer;
    /**
     * Mailer constructor.
     */
    public function __construct()
    {
        $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
            ->setUsername($this->username)
            ->setPassword($this->password);

        $this->mailer = \Swift_Mailer::newInstance($transporter);
    }


    public function sendEmail($email, $status)
    {
    }

    public function sendSuccessEmail($email)
    {
        $message = new \Swift_Message();
        $message->addTo($email);
        $message->addFrom($this->from);
        $message->setBody('you are the winner');
        $message->setSubject($this->subject);
        if ($this->mailer->send($message)) {
            return true;
        }

        return false;
    }

    public function sendFailedEmail($email)
    {
        //@todo add logic in case of invalid code
    }
}
