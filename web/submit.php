<?php

require_once '../vendor/autoload.php';
//@todo replace by autoloader
require_once '../src/CodeChecker.php';
require_once '../src/Mailer.php';

use Symfony\Component\HttpFoundation\Request;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Symfony\Component\HttpFoundation\Response;

$request = Request::createFromGlobals();

// allow only post method
if (!$request->isMethod(Request::METHOD_POST)) {
    $response = new Response();
    $response->setStatusCode(Response::HTTP_FORBIDDEN);
    $response->send();
    exit;
}


$logger = new Logger('code_logger');
$logger->pushHandler(new StreamHandler(__DIR__ . '/../logs/file.log'));

$mailer = new Mailer();



$file = $request->files;
$code = $request->get('code');
$email = $request->get('email');
$photo = $file->get('photo');

$codeChecker = new CodeChecker();
if ($codeChecker->isValid($code)) {
    // upload file
    $fileName = $code . '.' . $photo->guessExtension();
    $photo->move(__DIR__ . "/../files/", $fileName);

    // log valid code
    $logger->addInfo("Valid code '$code' entered");
    // send email
    $mailer->sendSuccessEmail($email);
    // invalidate code
    $codeChecker->invalidateCode($code);
    // show message
    echo 'Code successfully added';
} else {
    $logger->addInfo("Wrong code '$code' entered'");
    $mailer->sendFailedEmail($email);
    echo 'Code invalid';
}
